
#include <pybind11/pybind11.h>

#include "add.hpp"

PYBIND11_MODULE(my_cpp_module, m){
	using namespace pybind11::literals;
	m.def("add", &add, "i"_a, "j"_a);
}