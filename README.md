# Compilation

In order to compile the code, run the following:

1. Make sure that the `cmake`, `make` and `pybind11` packages are loaded (for non-local executions):

```console
ml cmake
ml make
ml pybind11
```

2. Inside this project's main directory (where `README.md` is located, probably `pybind_minimal_example/`), create `build/Debug` and nevigate to it:

```console
mkdir -p build/Debug
cd build/Debug
```

3. Compile the code:
```console
cmake ../../
make -j
```

4. Run the test:
```console
python3 ../../test/test.py
```

If everything went fine, a "Success!" message should be printed.

# pybind11 installation (for local compilation)

In order to install pybind11, [download it's git repository](https://github.com/pybind/pybind11/tree/stable).
Go to the main folder where the pybind11 cmakelist file is and do the following:

```console
mkdir build
cd build
cmake ..
make check -j 4
sudo make install
```

To compile this project, go to the build directory and type:

```console
cmake -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
```

The following can be used in the shell:

```console
cmake -DPYBIND11_PYTHON_VERSION=3.6 ..
```
