import numpy as np
import os 
import sys

# get this file's ('test.py') path
dir_path = os.path.dirname(os.path.realpath(__file__))

# import pybind11 generated module 
sys.path.append(f'{dir_path}/../build/Debug')
import my_cpp_module

assert(my_cpp_module.add(7,8)==7+8)
print("Success!")
